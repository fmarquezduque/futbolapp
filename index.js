const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./back-end/config');

const app = express();

app.use(cors());

mongoose.connect(config.MONGODB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
   .then(db => console.log('Db connected'))
   .catch(err => console.log(err));
// dir upload
app.use('public', express.static(`${__dirname}/back-end/upload/imgs`));
// For parsing application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Routes 
app.use(require('./back-end/routes/routes'));
// Settings
app.set('port', process.env.PORT || config.PORT);
// Starting the server
app.listen(app.get('port'), ()=>{
    console.log('server on port', app.get('port'));
});
