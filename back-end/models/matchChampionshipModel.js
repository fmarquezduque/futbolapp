const mongoose = require('mongoose');

const matchChampionshipSchema = new mongoose.Schema({
    
    stadium : {type: mongoose.Schema.Types.ObjectId, ref: 'Stadium'},
    team_home:{type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
    team_away: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
    championship:String,
    ref: String,
    goalHome: { type: Number, default: 0 },
    goalAway: { type: Number, default: 0 },
    date: Date,
    schedule: String,
    end:{type:Boolean, default:false}, // false: partido sin resultado y true: partido con resultado
    created_at: { 
        type: Date, 
        default: Date.now 
    }
});



const matchChampionshipModel = mongoose.model('MatchChampioship', matchChampionshipSchema);

module.exports = matchChampionshipModel;