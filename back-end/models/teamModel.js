const mongoose = require('mongoose');

const teamSchema = new mongoose.Schema({
    
    name: { type: String, required: true, unique: true },
    playerNumber: Number,
    coach: String,
    colorOne: String,
    colorTwo: String,
    category: String,
    created_at: { type: Date, default: Date.now },
 
});

const teamModel = mongoose.model('Team', teamSchema);

module.exports = teamModel;