const mongoose = require('mongoose');

const matchSchema = new mongoose.Schema({
    
    stadium : {type: mongoose.Schema.Types.ObjectId, ref: 'Stadium'},
    creator: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    participants: {type:[String]},
    date: Date,
    schedule: String,
    description: String, 
    public: {type:Boolean, default:true},
    created_at: { 
        type: Date, 
        default: Date.now 
    }
});



const matchModel = mongoose.model('Match', matchSchema);

module.exports = matchModel;