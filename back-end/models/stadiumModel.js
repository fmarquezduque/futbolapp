const mongoose = require('mongoose');

const stadiumSchema = new mongoose.Schema({
    
    owner: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: { type: String, required: true },
    price: { type: Number, default: 0, required: true },
    number_phone: String,
    description: String,
    location:String,
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    size:{ type: String, required: true },
    type:{ type: String, required: true }

});


const stadiumModel = mongoose.model('Stadium', stadiumSchema);

module.exports = stadiumModel;