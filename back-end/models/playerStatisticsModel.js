const mongoose = require('mongoose');

const playerStatisticsSchema = new mongoose.Schema({
    
    player: {type: mongoose.Schema.Types.ObjectId, ref: 'Player'},
    championship: String,
    goals: { type: Number, default: 0 },
    yellowCard: { type: Number, default: 0 },
    redCard: { type: Number, default: 0 }
    
});



const playerStatisticsModel = mongoose.model('PlayerStatistics', playerStatisticsSchema);

module.exports = playerStatisticsModel;