const mongoose = require('mongoose');

const playerSchema = new mongoose.Schema({    
    name: String,
    position: { type: String, required: true },
    dorsal: String,
    team: {type: mongoose.Schema.Types.ObjectId, ref: 'Team'}
});


const playerModel = mongoose.model('Player', playerSchema);

module.exports = playerModel;