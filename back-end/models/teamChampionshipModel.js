const mongoose = require('mongoose');

const teamChampionshipSchema = new mongoose.Schema({    
    
    team: String,
    championship: String

});


const teamChampionshipModel = mongoose.model('teamChampionship', teamChampionshipSchema);

module.exports = teamChampionshipModel;