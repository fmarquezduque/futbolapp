const mongoose = require('mongoose');

const championshipSchema = new mongoose.Schema({
    
    name : {type: String, unique: true},
    season: {type: String},
    numberTeam: Number,
    date_start: Date,
    date_end: Date,
    category: String,
    created_at: { 
        type: Date, 
        default: Date.now 
    }
});



const championshipModel = mongoose.model('Championship', championshipSchema);

module.exports = championshipModel;