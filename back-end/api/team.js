const express = require('express');
const Team = require('../models/teamModel');
const Player = require('../models/playerModel');
const matchChampionship = require('../models/matchChampionshipModel');
const {isAuth, isAdmin} = require('../util');
const router = express.Router();

// Obtener todos los equipos
router.get('/teams',isAuth, async (req, res) => {
    
    try{
        const teams = await Team.find();

        if(teams)
           res.send({teams});
        else
           res.status(404).send({message:'No hay equipos creados'});
    }catch(e){
        res.send(e);
    }  
});

// Obtener un equipo por su id
router.get('/team/', isAuth, async (req, res) => {

    try{    
        const teamId = await Team.findById({_id:req.query.idteam});

        if(teamId)
            res.send({teamId});
        else
            res.status(404).send({message:'Equipo no encontrado'});
    
    }catch(e){
        res.send(e);
    }
});

router.post('/register/team' , isAuth, isAdmin, async (req, res) => {

    try{
        const nameTeam = await Team.findOne({name: req.body.name});

        if(!nameTeam){

            const team = new Team({
                name: req.body.name,
                playerNumber: req.body.playerNumber,
                coach: req.body.coach,
                colorOne: req.body.colorOne,
                colorTwo: req.body.colorTwo,
                category: req.body.category
            });
        
            const newTeam = await team.save();
            
            if(newTeam){
                res.status(200).send({data: newTeam.name});
            }else{
                res.status(500).send({message:'No se ha podido registrar el equipo'});
            }
        }else{
            res.status(400).send({message:'Ya existe un equipo con este nombre'});
        }

    }catch(e){
        res.send(e);
    }
});


router.put('/update/team/', isAuth, isAdmin , async (req, res) => {
    
    try{    
        const team = await Team.findById({_id:req.query.idteam});

        if(team){
            team.name = req.body.name;
            team.playerNumber = req.body.logo;
            team.coach = req.body.coach; 
            team.colorOne = req.body.colorOne;
            team.colorTwo = req.body.colorTwo;
            team.category = req.body.category;
            
            const updatedTeam = await team.save();
            res.send({updatedTeam});
        }else{
            res.status(404).send({ message: 'Equipo no encontrado' });
        }  
    
    }catch(e){
        res.send(e);
    }
});

router.delete('/team/', isAuth, isAdmin, async (req, res) => {
   
    try{
        const deletedTeam = await Team.findById(req.query.idteam);

        if (deletedTeam) {
            
            const idTeam = deletedTeam._id;

            // Eliminamos los jugadores que pertenecian al equipo que se esta eliminado.
            
            await Player.deleteMany({team:idTeam});

            // Eliminar todos los partidos que tiene registrado este equipo

            await matchChampionship.deleteMany({$or:[{team_home: idTeam},{team_away: idTeam}]});

            await deletedTeam.remove();
            res.send({ message: 'Equipo eliminado' });
        } else {
            res.send('Error en la eliminación.');
        }
    }catch(e){
        res.send(e);
    }
});





module.exports = router;