const express = require('express');
const Match = require('../models/matchModel');
const matchChampionship = require('../models/matchChampionshipModel');
const {isAuth} = require('../util');
const router = express.Router();



router.get('/', isAuth ,async(req,res)=>{
    
    try{
        const stadium =  {path: 'stadium', select: 'name'};
        const creator =  {path: 'creator', select: 'name'};

        await Match.find().populate(creator).populate(stadium).exec(function (err, match){

            if(match)
            res.send(match);
            else
            res.send(err);

        });
    }catch(e){
        res.send(e);
    }
    
});

router.get('/match/', isAuth ,async(req,res)=>{
    
    try{
        const stadium =  {path: 'stadium', select: 'name'};
        const creator =  {path: 'creator', select: 'name'};

        await Match.find({_id:req.query.idmatch}).populate(creator).populate(stadium).exec(function (err, match){

            if(match)
            res.send(match);
            else
            res.send(err);

        });
    }catch(e){
        res.send(e);
    }
    
});

router.get('/user/match/', isAuth ,async(req,res)=>{
    
    
    try{
        
        const stadium =  {path: 'stadium', select: 'name'};

        await Match.find({creator:req.user._id}).populate(stadium).exec(function (err, match){
            if(match)
            res.send(match);
            else
            res.send(err);

        });
    }catch(e){
        res.send(e);
    }
});

router.post('/join/match/', isAuth ,async(req,res)=>{

    
    try{
        
        const matchId = await Match.findById({_id:req.query.idmatch});

        if(matchId){
            matchId.participants = req.user._id;
            const updatedMatch= await matchId.save();
        
            res.send({updatedMatch});
        }else{
            re.status(404).send({message:'No se encontró el encuentro'});
        }
    }catch(e){
        res.send(e);
    }

});


router.post('/register/match', isAuth, async (req, res) => {

    
    try{
        
        const matchexist = await Match.find({ 
            $and: [{ stadium:req.body.stadium }, { date: req.body.date }, { schedule: req.body.schedule }]
        });

        const matchChampexist = await matchChampionship.find({ 
            $and: [{ stadium:req.body.stadium }, { date: req.body.date }, { schedule: req.body.schedule }]
        });

        if(Object.keys(matchexist).length === 0 && Object.keys(matchChampexist).length === 0 ){
            
            const match = new Match({
                stadium: req.body.stadium,
                creator: req.user._id,
                date: req.body.date,
                schedule: req.body.schedule,
                description: req.body.description,
                public: req.body.public
            });
               
            const newMatch = await match.save();
               
            if(newMatch){
                res.status(200).send({data: newMatch});
            }else{
                res.status(500).send({message:'No se ha podido registrar el encuentro'});
            }
        }else{
            res.send({message:'Estadio se encuentra ocupado para esta fecha'});
        }
    }catch(e){
        res.send(e);
    }
        
    
    
});

router.put('/update/match/', isAuth, async (req, res) => {

    try{
        const matchId = await Match.findById({_id:req.query.idmatch});

        if(matchId){

            matchId.stadium = req.body.stadium; 
            matchId.date = req.body.date; 
            matchId.schedule = req.body.schedule;
            matchId.description = req.body.description; 
            matchId.public = req.body.public;

            const updatedMatch= await matchId.save();

            res.send({updatedMatch});

        }else{
            res.status(500).send({message:'No se ha podido encontrar el encuentro'});
        }
    }catch(e){
        res.send(e);
    }

    
    
});

router.delete('/match/', isAuth, async (req, res) => {
   
    try{
        const deletedMatch = await Match.findById(req.query.idmatch);

        if(deletedMatch){
            await deletedMatch.remove();
            res.send({message:'Partido eliminado'});
        }else{
            res.send('Error en la eliminación');
        }

    }catch(e){
        res.send(e)
    }
  
});


module.exports = router;