const express = require('express');
const User = require('../models/userModels');
const bcrypt = require('bcryptjs');
const {getToken, isAuth} = require('../util');
const router = express.Router();
const SALT = 10;

router.get('/profile', isAuth, async (req, res) =>{
  res.status(200).send(req.user);
});

router.put('/update/user/', isAuth, async (req, res) => {

  try{
      
      let passw;

      const user = await User.findById({_id: req.user._id});
      
      if(req.body.password==="")
          passw = req.body.password;
      else
          passw = await bcrypt.hashSync(req.body.password,SALT);
          
      if(user){
        
        user.name = req.body.name || req.user.name;
        user.email = req.body.email || req.user.email;
        user.password = passw || req.user.password;
        user.number_phone = req.body.number_phone || req.user.number_phone;
        
        const updatedUser = await user.save();
        
        res.status(200).send({
          _id: updatedUser.id,
          name: updatedUser.name,
          email: updatedUser.email,
          number_phone: updatedUser.number_phone,
          isAdmin: updatedUser.isAdmin,
          token: getToken(updatedUser),
        });
      }else{
        res.status(400).send({ message: 'Usuario no encontrado' });
      }
  }catch(e){
    res.send(e);
  }
  
});

router.post('/login', async (req, res) => {

  try{
      const signinUser = await User.findOne({
        email: req.body.email,
      });

      if(signinUser){
        const passwcompare = bcrypt.compareSync(req.body.password,signinUser.password);

        if(signinUser && passwcompare ){
          res.json({
            _id: signinUser.id,
            name: signinUser.name,
            email: signinUser.email,
            isAdmin: signinUser.isAdmin,
            token: getToken(signinUser),
          });
        }else{
          res.status(400).send({ message: 'Contraseña incorrecta' });
        }
      }else{
        res.status(400).send({ message: 'Email incorrecto' });
      }
  }catch(e){
    res.send(e);
  }
  
});

router.post('/signup', async (req, res) => {
  
  try{
        
      const emailexist = await User.findOne({
        email:req.body.email
      });

      if(!emailexist){
        
        const passw = await bcrypt.hashSync(req.body.password,SALT);

        const user = new User({
          name: req.body.name,
          email: req.body.email,
          password: passw,
          number_phone: req.body.number_phone,
        });
        const newUser = await user.save();
        if (newUser) {
          res.status(200).send({
            _id: newUser.id,
            name: newUser.name,
            email: newUser.email,
            number_phone: newUser.number_phone,
            isAdmin: newUser.isAdmin,
            token: getToken(newUser),
          });
        }else{
          res.status(400).send({ message: 'Datos invalido.' });
        }
      }else{
        res.status(400).send({message:'Ya existe un usuario con este email.'})
      } 
  }catch(e){
    res.send(e);
  } 
});


module.exports = router;
