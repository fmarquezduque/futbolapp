const express = require('express');
const PlayerStatistics = require('../models/playerStatisticsModel');
const Player = require('../models/playerModel');
const Championship = require('../models/championshipModel');
const {isAuth, isAdmin} = require('../util');
const router = express.Router();


// Registrar a un jugador en las estadisticas

router.post('/player/register/statistics/', isAuth, isAdmin, async (req, res) => {
    
    try{
        
        const playerExist = await Player.findById(req.query.idplayer);
        const championshipExist = await Championship.find({name:req.body.championship});

        const PlayerStatExist = await PlayerStatistics.find({$and:[{player:req.query.idplayer},{championship:req.body.championship}]}); 
        
        if(playerExist && Object.keys(championshipExist).length !== 0 && Object.keys(PlayerStatExist).length === 0){

            const playerStat = new PlayerStatistics({
                player: playerExist._id,
                championship: req.body.championship,
                goals: req.body.goals,
                yellowCard: req.body.yellowCard,
                redCard: req.body.redCard
            });
            
            const newPlayerStat = await playerStat.save();
           
            if(newPlayerStat){
               res.status(200).send({data: newPlayerStat});
            }else{
               res.status(500).send({message:'No se ha podido registrar el partido'});
            }
        }else{
            res.status(404).send({message:'El jugador y/o el equipo no existen'});
        }
        
    }catch(e){
        res.send(e);
    }  
});



// Obtener las estadisticas de un jugador por su id

router.get('/player/statistics/', isAuth, async (req, res) => {

    try{    
        const PlayerStat = await PlayerStatistics.find({player:req.query.idplayer});

        if(PlayerStat)
            res.send(PlayerStat);
        else
            res.send('No hay estadisticas registradas');

    }catch(e){
        res.send(e);
    }
});


// Obtener tabla de goleadores en un campeonato
router.get('/player/statistics/championship/goals/', isAuth, async (req, res) => {

    try{    
       
        const PlayerStatChamp = await PlayerStatistics.find({championship:req.query.nameChampionship}).sort({goals:-1});
        
        if(PlayerStatChamp)
            res.send(PlayerStatChamp);
        else
            res.send('No hay estadisticas registradas');
    
    }catch(e){
        res.send(e);
    }
});

module.exports = router;