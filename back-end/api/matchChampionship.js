const express = require('express');
const matchChampionship = require('../models/matchChampionshipModel');
const match = require('../models/matchModel');
const Championship  = require('../models/championshipModel');
const Team = require('../models/teamModel');
const {isAuth, isAdmin} = require('../util');
const router = express.Router();

// Todos los partidos registrados

router.get('/matchChampionships', isAuth ,async(req,res)=>{

    try{
        const stadium =  {path: 'stadium', select: 'name'};
        const team_home =  {path: 'team_home', select: 'name'};
        const team_away =  {path: 'team_away', select: 'name'};

        await matchChampionship.find().populate(stadium).populate(team_home).populate(team_away).exec(function (err, matchchampionship){
            if(matchchampionship)
                res.send(matchchampionship);
            else
                res.send(err);
        });
    }catch(e){
        res.send(e);
    }
    
});

// Partido por su id
router.get('/matchChampionship/', isAuth ,async(req,res)=>{

    try{
        const stadium =  {path: 'stadium', select: 'name'};
        const team_home =  {path: 'team_home', select: 'name'};
        const team_away =  {path: 'team_away', select: 'name'};

        await matchChampionship.findById(req.query.idmatchchampionship).populate(stadium).populate(team_home).populate(team_away).exec(function (err, matchchampionship){
            if(matchchampionship)
                res.send(matchchampionship);
            else
                res.send(err);
        });
    }catch(e){
        res.send(e);
    }
    
});

// Todos los partidos de un campeonato

router.get('/championship/matchChampionships/', isAuth ,async(req,res)=>{

    try{
        const stadium =  {path: 'stadium', select: 'name'};
        const team_home =  {path: 'team_home', select: 'name'};
        const team_away =  {path: 'team_away', select: 'name'};

        await matchChampionship.find({championship:req.query.championship}).populate(stadium).populate(team_home).populate(team_away).exec(function (err, matchchampionship){
            if(matchchampionship)
                res.send(matchchampionship);
            else
                res.send(err);
        });
    }catch(e){
        res.send(e);
    }    
});

router.post('/register/matchChampionship', isAuth , isAdmin ,async (req, res) => {

    try{

        const team_homeCategory = await Team.findById(req.body.team_home);
        const team_awayCategory = await Team.findById(req.body.team_away);
        const championshipCategory = await Championship.findById(req.body.championship);

        const matchexist = await match.find({ 
            $and: [{ stadium:req.body.stadium }, { date: req.body.date }, { schedule: req.body.schedule }]
        });

        const matchChampexist = await matchChampionship.find({ 
            $and: [{ stadium:req.body.stadium }, { date: req.body.date }, { schedule: req.body.schedule }]
        });


        if(Object.keys(matchexist).length === 0 && Object.keys(matchChampexist).length === 0 && req.body.team_away !== req.body.team_home && team_homeCategory.category === championshipCategory.category && team_awayCategory.category === championshipCategory.category){
            
            const matchchampionship = new matchChampionship({
                stadium: req.body.stadium,
                team_home: req.body.team_home,
                team_away: req.body.team_away,
                championship: championshipCategory.name,
                ref: req.body.ref,
                date: req.body.date,
                schedule: req.body.schedule
            });
           
           const newmatchChampionship = await matchchampionship.save();
           
           if(newmatchChampionship){
               res.status(200).send({data: newmatchChampionship});
           }else{
               res.status(500).send({message:'No se ha podido registrar el partido'});
           }
        }else{
            res.status(400).send({message:'Error en los datos suministrados'});
        }
        
    } catch (e) {
        res.send(e);
    }
});


router.put('/update/matchChampionship/', isAuth ,isAdmin ,async (req, res) => {
  
    try{
        
        const matchchampionship = await matchChampionship.findById(req.query.idmatchChampionship);

        if(matchchampionship){
            
            matchchampionship.goalHome = req.body.goalHome;
            matchchampionship.goalAway = req.body.goalAway;
            matchchampionship.end      = true;

            const updatedmatchchampionship = await matchchampionship.save();
            res.send({updatedmatchchampionship});
        }else{
            res.status(404).send({ message: 'Partido no encontrado' });
        }  
    }catch(e){
        res.send(e);
    }
});

router.delete('/matchChampionship/', isAuth, isAdmin, async (req, res) => {

    try{
        const deletedMatchChampionship = await matchChampionship.findById(req.query.idmatchChampionship);

        if(deletedMatchChampionship){
            await deletedMatchChampionship.remove();
            res.send({ message: 'Partido eliminado' });
        }else{
            res.send('Error en la eliminación.');
        }

    }catch(e){
        res.send(e);
    }
});


// Todos los partidos de un equipo
router.get('/team/matchChampionships/', isAuth ,async(req,res)=>{

    try{
        const stadium   =  {path: 'stadium', select: 'name'};
        const team_home =  {path: 'team_home', select: 'name'};
        const team_away =  {path: 'team_away', select: 'name'};

        await matchChampionship.find({
            $or: [
              { team_home: req.query.idteam },
              { team_away: req.query.idteam}
            ]}).populate(stadium).populate(team_home).populate(team_away).exec(function (err, matchchampionship){
            if(matchchampionship)
                res.send(matchchampionship);
            else
                res.send(err);
        });
    }catch(e){
        res.send(e);
    }
});



module.exports = router;