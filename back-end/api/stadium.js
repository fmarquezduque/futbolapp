const express = require('express');
const Stadium = require('../models/stadiumModel');
const {isAuth} = require('../util');
const router = express.Router();

router.get('/user/stadiums/', isAuth, async(req, res)=>{

    try{
        const owner =  {path: 'owner', select: 'name'};

        await Stadium.find({owner:req.user._id}).populate(owner).exec(function (err, matches){

            if(matches)
              res.send(matches);
            else
              res.send(err);
        });
    }catch(e){
        res.send(e);
    }
});


router.get('/stadium/', isAuth, async(req, res)=>{

    try{
        const stadiumexist = req.query.idstadium;
        const owner =  {path: 'owner', select: 'name'};

        await Stadium.findById({_id:stadiumexist}).populate(owner).exec(function (err, matches){

            if(matches)
              res.send(matches);
            else
              res.send(err);
        });
    }catch(e){
        res.send(e);
    }
});

router.get('/stadiums', isAuth, async(req, res)=>{
     
    try{    
        const owner   =   {path: 'owner', select: 'name'};

        await Stadium.find().populate(owner).exec(function (err, stadium){

            if(stadium)
            res.send(stadium);
            else
            res.send(err);

        });
    }catch(e){
        res.send(e);
    }
});

router.post('/register/stadium', isAuth, async(req, res)=>{

    try{
        
        const stadium = new Stadium({
            owner: req.user._id,
            name: req.body.name,
            price: req.body.price,
            location: req.body.location,
            number_phone: req.body.number_phone,
            description: req.body.description,
            size: req.body.size,
            type: req.body.type
        });
        
        const newStadium = await stadium.save();
        
        if(newStadium){
            res.status(200).send({data: newStadium});
        }else{
            res.status(500).send({message:'No se ha podido registrar el estadio'});
        }

    }catch(e){
        res.send(e);
    }
    
});

router.put('/update/stadium/', isAuth, async(req, res)=>{

    try{
            
        const stadium = await Stadium.findOne({_id:req.query.idstadium, owner: req.user._id});

        if(stadium){
            stadium.name = req.body.name;
            stadium.price = req.body.price;
            stadium.description = req.body.description;
            stadium.location = req.body.location;
            stadium.number_phone = req.body.number_phone;
            stadium.size = req.body.size;
            stadium.type = req.body.type;

            const updateStadium = await stadium.save();
            res.status(200).send({data:updateStadium});
        }else{
            res.status(404).send({message:'No se encontró estadio con el id ingresado'});
        }
    }catch(e){
        res.send(e);
    }

});
router.delete('/stadium/', isAuth, async (req, res) => {
   
    try{
        const deletedStadium = await Stadium.findById(req.query.idstadium);

        if(deletedStadium){
            await deletedStadium.remove();
            res.send({message:'Estadio eliminado'});
        }else{
            res.send('Error en la eliminación');
        }

    }catch(e){
        res.send(e)
    }
  
});


module.exports = router;