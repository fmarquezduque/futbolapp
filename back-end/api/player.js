const express = require('express');
const Player = require('../models/playerModel');
const Team = require('../models/teamModel');
const { isAuth, isAdmin} = require('../util');
const router = express.Router();


router.get('/players', isAuth, async(req, res)=>{
    
    try{
        const team  = {path:'team', select:'name'};
        
        await Player.find().populate(team).exec(function(err,players){
            
            if(players)
               res.send(players);
            else
               res.send(err);
        });

    }catch(e){
        res.send(e);
    }
});


router.get('/player/', isAuth, async(req, res)=>{
    
    try{
        const team  = {path:'team', select:'name'};
        
        await Player.findById(req.query.idplayer).populate(team).exec(function(err,player){
            
            if(player)
               res.send(player);
            else
               res.send(err);
        });

    }catch(e){
        res.send(e);
    }
});

router.get('/team/players/', isAuth, async(req, res)=>{
    
    try{
        const team  = {path:'team', select:'name'};
        
        await Player.find({team:req.query.idteam}).populate(team).exec(function(err,player){
            
            if(player)
               res.send(player);
            else
               res.send(err);
        });

    }catch(e){
        res.send(e);
    }
});


router.post('/register/player', isAuth, isAdmin, async (req, res) => {
  
    try{
        
        // Para verificar de que no existan jugadores con el mismo dorsal

        const playerExist = await Player.find({ 
            $and: [{ dorsal:req.body.dorsal }, { team: req.body.team }]
        });
        
        // Número de jugadores en un equipo
        const TeamPlayers = await  Player.count({team:req.body.team});
        
        // Número de jugadores aceptados por un equipo
        const numberPlayer = await Team.findById(req.body.team);


        if(Object.keys(playerExist).length === 0 && (TeamPlayers < numberPlayer.playerNumber)){
            
            const player = new Player({
                name:req.body.name,
                position:req.body.position,
                dorsal:req.body.dorsal,
                team:req.body.team
            });
    
            const newPlayer = await player.save();
    
            if(newPlayer){
                res.status(200).send(newPlayer);
            }else{
                res.status(400).send({message:'No se ha podido registrar el jugador'});
            }
    
        }else{
            res.status(400).send({message:"Ya existe un jugador con este dorsal y/o se ha superado el número de jugadores permitido"});
        }
        
    }catch(e){
        res.send(e);
    }
});


router.put('/update/player/', isAuth, isAdmin, async (req, res) => {
   
    try{
        const playerId = await Player.findById({_id:req.query.idplayer});
        
        if(playerId){

            playerId.name = req.body.name;
            playerId.position = req.body.position;
            playerId.dorsal = req.body.dorsal;
            playerId.team = req.body.team; 

            const updatedPlayer = await playerId.save();

            res.send({updatedPlayer});

        }else{
            res.send({message:'Jugador no encontrado'});
        }

    }catch(e){
        res.send(e);
    }
  
});

router.delete('/player/', isAuth, isAdmin, async (req, res) => {
   
    try{
        const deletedPlayer = await Player.findById(req.query.idplayer);

        if(deletedPlayer){
            await deletedPlayer.remove();
            res.send({message:'Jugador eliminado'});
        }else{
            res.send('Error en la eliminación');
        }

    }catch(e){
        res.send(e);
    }
  
});


module.exports = router;
