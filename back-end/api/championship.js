const express = require('express');
const Championship = require('../models/championshipModel');
const matchChampionship = require('../models/matchChampionshipModel');
const {isAuth, isAdmin} = require('../util');
const router = express.Router();


// Obtener todos los campeonatos registrados

router.get('/championships', isAuth ,async(req,res)=>{
    
    try{     
        championships = await Championship.find();

        if(championships){
            res.send(championships);
        }else{
            res.send({message:'No hay campeonatos registrados'});
        }
    }catch(e){
        res.send(e);
    }
});

// Obtener un campeonato por su id
router.get('/championship/', isAuth ,async(req,res)=>{
    
    try{
        await Championship.find({_id:req.query.idchampionship}).exec(function (err, championship){

            if(championship)
              res.send(championship);
            else
              res.send(err);
    
        });
    }catch(e){
        res.send(e);
    }
});

router.post('/register/championship', isAuth ,isAdmin , async (req, res) => {
    
    try{
        const nameChampionship = await Championship.findOne({name: req.body.name});

        if(!nameChampionship){

            const championship = new Championship({
                name: req.body.name,
                season: req.body.season,
                numberTeam: req.body.numberTeam,
                date_start: req.body.date_start,
                date_end: req.body.date_end,
                category:req.body.category
            });
        
            const newChampionship = await championship.save();
            
            if(newChampionship){
                res.status(200).send(newChampionship);
            }else{
                res.status(500).send({message:'No se ha podido registrar el campeonato'});
            }
        }else{
            res.status(400).send({message:'Ya existe un campeonato con este nombre'});
        }
    }catch(e){
       res.send({e});
    } 
});


router.put('/update/championship/', isAuth, isAdmin, async (req, res) => {
  
    try{
            
        const championship = await Championship.findById({_id:req.query.idchampionship});

        if(championship){
            
            championship.name = req.body.name;
            championship.season = req.body.season;
            championship.numberTeam = req.body.numberTeam;
            championship.date_start =  req.body.date_start;
            championship.date_end = req.body.date_end;
            championship.category = req.body.category;
               
            const updatedChampionship = await championship.save();
            res.send({updatedChampionship});
        }else{
            res.status(404).send({ message: 'Campeonato no encontrado' });
        }     
    }catch(e){
        res.send({e});
    }
});

router.delete('/championship/', isAuth, isAdmin, async (req, res) => {
    try{
        const deletedChampionship = await Championship.findById(req.query.idchampionship);

        
        if (deletedChampionship) {

            const nameMatchChampionship = deletedChampionship.name;
            // Eliminamos los partidos que pertenecian al campeonato que se esta eliminado.
            
            await matchChampionship.deleteMany({championship: nameMatchChampionship});

            await deletedChampionship.remove();
            res.send({ message: 'Campeonato eliminado' });
        } else {
            res.send('Error en la eliminación.');
        }

    }catch(e){
        res.send(e);
    }
});

module.exports = router;