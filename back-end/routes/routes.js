const express = require('express');
const app = express();

app.use(require('../api/user'));
app.use(require('../api/match'));
app.use(require('../api/stadium'));
app.use(require('../api/championship'));
app.use(require('../api/matchChampionship'));
app.use(require('../api/team'));
app.use(require('../api/player'));
app.use(require('../api/playerStatistics'));

module.exports = app;