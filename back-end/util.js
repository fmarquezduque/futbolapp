const jwt = require('jsonwebtoken');
const config = require('./config');
const characterNumber = 7;

const getToken = (user) => {
  return jwt.sign(
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      password: user.password,
      number_phone: user.number_phone,
      isAdmin: user.isAdmin,
    },
    config.JWT_SECRET,
    {
      expiresIn: '1h',
    }
  );
};

const isAuth = (req, res, next) => {
  const token = req.headers.authorization;

  if (token) {
    const onlyToken = token.slice(characterNumber, token.length);
    jwt.verify(onlyToken, config.JWT_SECRET, (err, decode) => {
      if (err) {
        return res.status(401).json({ message: 'El token es invalido' });
      }
      req.user = decode;
      next();
      return;
    });
  } else {
    return res.status(401).json({ message: 'No esta autorizado.' });
  }
};


const isAdmin = (req, res, next) => {

  
  if (req.user && req.user.isAdmin) {
    return next();
  }
  return res.status(401).send({ message: 'Admin Token is not valid.' });
};


module.exports = { getToken, isAuth, isAdmin };