const chai = require("chai");
const chaitHttp = require("chai-http");
const request = require("supertest");
const expect = chai.expect;
const api = 'https://futbolamateurapp.herokuapp.com';
chai.use(chaitHttp);

let token = 'token';
let idchampionship = 'id';

const championshipTest = {

    name: "championshipTest",
    season: "2020-2",
    numberTeam: 10,
    date_start: "2020-11-10T00:00:00.000+00:00",
    date_end: "2020-12-19T00:00:00.000+00:00"
};

/*
    Iniciamos sesión para probar cada it.
*/

beforeEach((done) => {
    request(api)
    .post('/login')
    .send({
        email:"user@futbolapp.com",
        password:"userpassword",
    })
    .end((err, response) => {
        token = response.body.token; // save the token!
        done();
    });
});


/*
    Pruebas a la rutas que son solo accedidas por un Admin
*/

describe("User Admin unit test", ()=>{

    it("Should return status 200 because admin is correct - register championship", (done) => {

        request(api)
        .post('/register/championship')
        .send(championshipTest)
        .set({'Authorization': 'Bearer '+token})
        .expect(200)
        .end((err, res) => {
            idchampionship = res.body._id;
            done();
        });
    });


    it("Should return status 200 because admin is correct - update championship", async () => {

        championshipTest.name = "championshipTestUpdate";


        let res = await chai
        .request(api)
        .put(`/update/championship/?idchampionship=${idchampionship}`)
        .send(championshipTest)
        .set({'Authorization': 'Bearer '+token});

        
        expect(res.status).to.equal(200);

    });

    // Eliminar campeonato de prueba
    
    it("Should return status 200 because admin is correct - delete championship", async () => {

        let res = await chai
        .request(api)
        .delete(`/championship/?idchampionship=${idchampionship}`)
        .set({'Authorization': 'Bearer '+token});

        expect(res.status).to.equal(200);

    });

});



