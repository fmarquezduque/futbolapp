const chai = require("chai");
const chaitHttp = require("chai-http");
const request = require("supertest");
const expect = chai.expect;
const api = 'https://futbolamateurapp.herokuapp.com';
chai.use(chaitHttp);
let token = 'token';

// usuario para hacer pruebas

let userTest =  {
    name:"user", 
    email:"user@futbolapp.com", 
    password:"userpassword", 
    number_phone:"333"
};

/*
    Iniciamos sesión para probar cada it.
*/

beforeEach((done) => {
    request(api)
    .post('/login')
    .send({
        email:"user@futbolapp.com",
        password:"userpassword",
    })
    .end((err, response) => {
        token = response.body.token; // save the token!
        done();
    });
});

/*
    Pruebas a la ruta de usuario
*/

describe("User unit test", ()=>{

    /* 
        Test a registrar usuario utilizando 
        la libreria chai para hacer peticiones
        http.

    */

    it("should return status 400 because user exist - signup", async () => {

        let res = await chai
        .request(api)
        .post('/signup')
        .send(userTest);

        expect(res).to.have.status(400);
        

    });

    /* 
        El token es invalido
    */
    it("should return status 401 because token is not correct - profile", async () => {

        let res = await chai
        .request(api)
        .get('/profile')
        .set({'Authorization': 'Bearer '+'token'});

        expect(res.status).to.equal(401);

    });


    it("should return status 401 because token is not correct - update", async () => {
        
        userTest.name = "userUpdate";

        let res = await chai
        .request(api)
        .put('/update/user')
        .set({'Authorization': 'Bearer '+'token'})
        .send(userTest);

        expect(res.status).to.equal(401);

    });

    /*
        Token proveido
    */

    it("should return status 200 because token is  correct - profile", async () => {

        let res = await chai
        .request(api)
        .get('/profile')
        .set({'Authorization': 'Bearer '+token});


        expect(res.status).to.equal(200);

    });


    it("should return status 200 because token is correct - update", async () => {
        
        userTest.name = "userUpdate";

        let res = await chai
        .request(api)
        .put('/update/user')
        .set({'Authorization': 'Bearer '+token})
        .send(userTest);

        expect(res.status).to.equal(200);
        expect(res.body.name).to.equal("userUpdate");

    });

});

