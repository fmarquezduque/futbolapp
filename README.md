# FútbolApp

FútbolApp es una red social de fútbol amateur que cuenta con una comunidad de jugadores, árbitros y administradores de la plataforma. A través de la plataforma se pueden gestionar partidos, campeonatos y otras cosas más.

### Admin

"name":"Admin",
"email":"admin@gmail.com",
"password":"admin",
"number_phone":"32358752"

# Rutas de la API

- https://futbolamateurapp.herokuapp.com/

### User

- https://futbolamateurapp.herokuapp.com/profile (Obtener un usuario)(GET)
- https://futbolamateurapp.herokuapp.com/user/stadiums (Obtener los estadios registrados por un usuario)(GET)
- https://futbolamateurapp.herokuapp.com/login (ingresar a la plataforma)(POST)
- https://futbolamateurapp.herokuapp.com/signup (Registrarse en la platafora)(POST)
- https://futbolamateurapp.herokuapp.com/update/user (Actualizar usuario por su id)(PUT)

### Stadium

- https://futbolamateurapp.herokuapp.com/register/stadium (registrar estadio)(POST)
- https://futbolamateurapp.herokuapp.com/stadium/?idstadium="_id" (Obtener estadio por id)(GET)
- https://futbolamateurapp.herokuapp.com/stadiums (Obtener todos los estadio)(GET)
- https://futbolamateurapp.herokuapp.com/update/stadium/?idstadium="_id" (Actualizar estadio por su id)(PUT)
- https://futbolamateurapp.herokuapp.com/stadium/?idstadium="_id" (Actualizar estadio por su id)(DELETE)

### Booking

- https://futbolamateurapp.herokuapp.com/register/booking (registrar reserva)(POST)
- https://futbolamateurapp.herokuapp.com/stadium/booking/?idstadium="_id" (Obtener todas las reserva por id de un estadio)(GET)
- https://futbolamateurapp.herokuapp.com/booking/?idbooking="_id" (Obtener reserva por id)(GET)
- https://futbolamateurapp.herokuapp.com/user/booking (Obtener todas las reserva por usuario que este logueado)(GET)
- https://futbolamateurapp.herokuapp.com/update/booking/?idbooking="_id" (Actualizar reserva por su id)(PUT)

### Match

- https://futbolamateurapp.herokuapp.com/ (Todos los partidos registrados en el sistema) (GET)
- https://futbolamateurapp.herokuapp.com/register/match (registrar partido)(POST)
- https://futbolamateurapp.herokuapp.com/stadium/match/?idmatch="_id" (Obtener el partido con este id)(GET)
- https://futbolamateurapp.herokuapp.com/user/match (Obtener todos los partidos por el usuario que este logueado)(GET)
- https://futbolamateurapp.herokuapp.com/update/match/?idmatch="_id" (Actualizar partido por su id, por el usuario que lo creo)(PUT)
- https://futbolamateurapp.herokuapp.com/join/match/?idmatch="_id" (Unirse a un partido)(POST)
- https://futbolamateurapp.herokuapp.com/match/?idmatch="_id" (Eliminar partido)(DELETE)

### Team

- https://futbolamateurapp.herokuapp.com/register/team (registrar equipos)(POST)
- https://futbolamateurapp.herokuapp.com/update/team/?idteam="_id" (Actualizar equipos)(PUT)
- https://futbolamateurapp.herokuapp.com/teams (Mostar todos los equipos)(GET)
- https://futbolamateurapp.herokuapp.com/team/?idteam="_id" (Mostrar un equipo)(GET)
- https://futbolamateurapp.herokuapp.com/team/?idteam="_id" (Eliminar un equipo)(DELETE)

### Championship

- https://futbolamateurapp.herokuapp.com/championships (Ver todos campeonatos)(GET)
- https://futbolamateurapp.herokuapp.com/championship/?idchampionship="_id" (Ver un campeonato)(GET)
- https://futbolamateurapp.herokuapp.com/register/championship (registrar campeonatos)(POST)
- https://futbolamateurapp.herokuapp.com/update/championship/?idchampionship="_id" (Actualizar campeonato)(PUT)
- https://futbolamateurapp.herokuapp.com/championship/?idchampionship="_id" (Eliminar campeonato)(DELETE)

### Player

- https://futbolamateurapp.herokuapp.com/players(Ver todos los jugadores)(GET)
- https://futbolamateurapp.herokuapp.com/team/players/?idteam="_id" (Ver todos los jugador de un equipo)(GET)
- https://futbolamateurapp.herokuapp.com/player/?idplayer="_id" (Ver un jugador)(GET)
- https://futbolamateurapp.herokuapp.com/championship/?idchampionship="_id" (Ver un campeonato)(GET)
- https://futbolamateurapp.herokuapp.com/register/player (registrar jugador)(POST)
- https://futbolamateurapp.herokuapp.com/update/player/?idplayer="_id" (Actualizar jugador)(PUT)
- https://futbolamateurapp.herokuapp.com/player/?idplayer="_id" (Eliminar jugador)(DELETE)

### MatchChampionship

- https://futbolamateurapp.herokuapp.com/matchChampionships (Ver todos los partidos)(GET)
- https://futbolamateurapp.herokuapp.com/matchChampionships/?idmatchchampionship="_id" (Ver un  partido)(GET)
- https://futbolamateurapp.herokuapp.com/championship/matchChampionships/?championship="name" (Ver todos los  partidos de un campeonato)(GET)
- https://futbolamateurapp.herokuapp.com/team/matchChampionships/?idteam="_id" (Ver todos los  partidos de un equipo)(GET)
- https://futbolamateurapp.herokuapp.com/register/matchChampionship (registrar partido)(POST)
- https://futbolamateurapp.herokuapp.com/update/matchChampionship/?idmatchChampionship="_id" (Actualizar partido)(PUT)
- https://futbolamateurapp.herokuapp.com/matchChampionship/?idmatchChampionship="_id" (Elimnar partido)(DELETE)
